from app import db



class JsonMixin(object):
    def from_db(self):
       return {col.name : getattr(self, col.name) for col in self.__table__.columns}

    def to_db(self, note):
        for col in self.__table__.columns:
            if col.name in note:
                setattr(self, col.name, note[col.name])

class ServiceDb(db.Model,JsonMixin):
    id = db.Column(db.Integer(), primary_key = True)
    fio = db.Column(db.String(30), nullable = False)
    ph_num = db.Column(db.String(15), nullable = False)
    tool_name = db.Column(db.String(30), nullable = False)
    ser_num = db.Column(db.String(40), unique = True, nullable = False)

    def __repr__(self):
        return "<{}: ser.num. - {}".format(self.fio, self.ser_num)
