from app import db, app
from app.models import ServiceDb
from flask import jsonify, url_for, request
from app.errors import bad_request

@app.route('/notes')
def show_all():
    all = ServiceDb.query.all() or {}
    print(all)
    all_data = [one_note.from_db() for one_note in all]
    return jsonify(all_data)

@app.route('/add', methods = ['POST'])
def add_note():
    new_note = ServiceDb()
    data = request.get_json()
    if not data:
        return bad_request("no data was recieved")
    new_note.to_db(data)
    db.session.add(new_note)
    db.session.commit()

    response = jsonify(new_note.from_db())
    response.status_code = 201
    response.headers['Location'] = url_for('show_note_id', id=new_note.id)
    return response

@app.route('/notes/<int:id>')
def show_note_id(id):
    return jsonify(ServiceDb.query.get_or_404(id).from_db())

@app.route('/notes/delete')
def delete_all():
    for one in ServiceDb.query.all():
        db.session.delete(one)
    db.session.commit()
    return 'sucess'
@app.route('/notes/delete/<int:id>')
def delete_id(id):
    note = ServiceDb.query.get(id)
    if note:
        res = jsonify(note.from_db())
        res.status_code = 200
        response.headers['Location'] = url_for('show_all')
        db.session.delete(note)
        db.session.commit()
        return res
    else:
        return bad_request("no note with {} id".format(id))

    
